import React from 'react';

import styled from 'styled-components';

import BarChart from '../components/BarChart';
import { getDummyData } from '../utils';

const BulletRating = () => {
  const [data, setData] = React.useState(() => getDummyData(5));

  const updateData = () => setData(() => getDummyData(5));

  return (
    <Container>
      <Title>Vanilla D3</Title>
      <ButtonRow>
        <button onClick={updateData}>Update Data</button>
      </ButtonRow>
      <BarChart data={data} />
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100vw;
  max-width: 800px;
  margin: 0 auto;
`;

const Title = styled.h2`
  font-family: Roboto, sans-serif;
  -webkit-font-smoothing: antialiased;
  font-size: 3.75rem;
  line-height: 3.75rem;
  font-weight: 300;
  margin: 3.5rem 0;
`;

const ButtonRow = styled.div`
  display: flex;
  flex: 1;
`;

export { BulletRating as default };
