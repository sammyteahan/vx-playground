import React from 'react';

import styled from 'styled-components';

import Card from '../components/common/Card';

const Home = () => (
  <CenteredContainer>
    <Card>
      <Card.Header>
        <h4>Rating</h4>
      </Card.Header>
      <Card.Body>
        <h4>Body</h4>
      </Card.Body>
      <Card.Footer>
        <h6>@sammyteahan</h6>
      </Card.Footer>
    </Card>
  </CenteredContainer>
);

/**
 * Styles
 */
const CenteredContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center
  height: 100%;
  background: ${({ theme }) =>
    `linear-gradient(
      to right,
      ${theme.colorGradientStart},
      ${theme.colorGradientMiddle},
      ${theme.colorGradientEnd}
    )`};
`;

const Squeeze = styled.div`
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
  position: relative;
  height: 100%;
  width: 100%;
`;

const StyledTitle = styled.h1`
  font-size: 44px;
  color: white;
`;

export { Home as default };
