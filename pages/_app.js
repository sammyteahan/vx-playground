import App, { Container } from "next/app"

import { createGlobalStyle } from "styled-components"

import Page from '../components/Page';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  html {
    height: 100%;
    min-height: 100%;
    box-sizing: border-box;
  }

  body {
    background: #fff;
    font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-style: normal;
    font-weight: 400;
    letter-spacing: 0;
    height: 100%;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -moz-font-feature-settings: "liga" on;
  }

  #__next {
    height: 100%;
  }
`

class MyApp extends App {
  render() {
    const { Component } = this.props
    return (
      <Container>
        <GlobalStyle />
        <Page>
          <Component {...this.props} />
        </Page>
      </Container>
    )
  }
}

export default MyApp;
