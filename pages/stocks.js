import React from 'react';

// data
import { appleStock } from '@vx/mock-data';
import { scaleTime, scaleLinear } from '@vx/scale';
import { extent, max } from 'd3-array';

// viz
import { Group } from '@vx/group';
import { AreaClosed } from '@vx/shape';
import { AxisBottom, AxisLeft } from '@vx/axis';
import { GridRows, GridColumns } from '@vx/grid';
import { withScreenSize } from '@vx/responsive';
import styled from 'styled-components';

// accessors
const xStock = d => new Date(d.date);
const yStock = d => d.close;

class Stocks extends React.Component {
  state = {
    margin: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  };

  renderChart = ({ data }) => {
    const { margin } = this.state;
    const { screenWidth, screenHeight } = this.props;

    const width = (screenWidth * 0.6) - margin.left - margin.right;
    const height = (screenHeight * 0.45) - margin.top - margin.bottom;

    // define bounds
    const xMax = width - margin.left - margin.right;
    const yMax = height - margin.top - margin.bottom;

    // define scales
    const xScale = scaleTime({
      range: [0, xMax],
      domain: extent(data, xStock),
    });

    const yScale = scaleLinear({
      range: [yMax, 0],
      /**
       * this is formats the chart nicely against the bottom of the container instead of the top,
       * therefore gives some nice margin at the top
       */
      // domain: [0, max(data, yStock)], // old, take up entire height
      domain: [0, max(data, yStock) + yMax / 3],
      nice: true // rounds values which can help the internal d3 stuff with axis values
    });

    return (
      <svg width={width} height={height}>
        <rect x={0} y={0} width={width} height={height} fill="#32deaa" rx={10} />
        <defs> {/* gradient defs */}
          <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
            <stop offset="0%" stopColor="#fff" stopOpacity={1} />
            <stop offset="100%" stopColor="#fff" stopOpacity={0.2} />
          </linearGradient>
        </defs>
        <Group>
          <GridRows
            lineStyle={{ pointerEvents: 'none' }}
            scale={yScale}
            width={xMax}
            strokeDasharray="2,2"
            stroke="rgba(255,255,255,0.3)"
          />
          <GridColumns
            lineStyle={{ pointerEvents: 'none' }}
            scale={xScale}
            height={yMax}
            strokeDasharray="2,2"
            stroke="rgba(255,255,255,0.3)"
          />
          <AreaClosed
            data={data}
            x={d => xScale(xStock(d))}
            y={d => yScale(yStock(d))}
            yScale={yScale}
            strokeWidth={1}
            stroke={'url(#gradient)'}
            fill={'url(#gradient)'}
          />
        </Group>
      </svg>
    );
  }

  render() {
    const data = appleStock;
    console.log(data);

    return (
      <Container>
        {this.renderChart({ data })}
      </Container>
    );
  }
}

/**
 * Styles
 */
const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  padding-top: 10rem;
`;

export default withScreenSize(Stocks);
