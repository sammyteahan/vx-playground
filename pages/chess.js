import React from 'react';

// data
const chessData = require('../data/chess-data.json');

import { compose } from 'redux'; // ya, I really just did that for this
import { appleStock } from '@vx/mock-data';
import { scaleTime, scaleLinear } from '@vx/scale';
import { extent, max, bisector } from 'd3-array';
import { timeFormat } from 'd3-time-format';

// viz
import { Group } from '@vx/group';
import { AreaClosed, Bar } from '@vx/shape';
import { AxisBottom, AxisLeft } from '@vx/axis';
import { GridRows, GridColumns } from '@vx/grid';
import { withTooltip, Tooltip } from '@vx/tooltip';
import { localPoint } from '@vx/event';
import { withScreenSize } from '@vx/responsive';
import styled from 'styled-components';

import HoverLine from '../components/HoverLine';
import MaxRating from '../components/MaxRating';

// accessors
const xRating = d => new Date(d.date);
const yRating = d => d.my_elo;

// utils
// get the index of the point in our data that we are closest to on hover
const bisectDate = bisector(d => xRating(d)).left;
const formatDate = timeFormat("%b %d, %y");

class Chess extends React.Component {
  state = {
    margin: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  };

  handleTooltip = ({ event, data, xRating, xScale, yScale }) => {
    const { showTooltip } = this.props;
    const { x } = localPoint(this.svg, event);
    const x0 = xScale.invert(x);
    const index = bisectDate(data, x0, 1);
    const d0 = data[index - 1]; // get the data point to the left
    const d1 = data[index]; // get the point to the right
    // get whichever point is greatest
    let d = x0 - xScale(xRating(d0)) > xScale(xRating(d1)) - x0 ? d1 : d0;

    showTooltip({
      tooltipLeft: xScale(xRating(d)),
      tooltipTop: yScale(yRating(d)),
      tooltipData: d,
    });
  }

  renderChart = ({ data }) => {
    const { margin } = this.state;
    const {
      screenWidth,
      screenHeight,
      hideTooltip,
      tooltipData,
      tooltipTop,
      tooltipLeft,
      events,
    } = this.props;

    const width = (screenWidth * 0.6) - margin.left - margin.right;
    const height = (screenHeight * 0.45) - margin.top - margin.bottom;

    // define bounds
    const xMax = width - margin.left - margin.right;
    const yMax = height - margin.top - margin.bottom;

    // define scales
    const xScale = scaleTime({
      range: [0, xMax],
      domain: extent(data, xRating),
    });

    const yScale = scaleLinear({
      range: [yMax, 0],
      /**
       * this is formats the chart nicely against the bottom of the container instead of the top,
       * therefore gives some nice margin at the top
       */
      domain: [0, max(data, yRating) + yMax / 2],
      nice: true // rounds values which can help the internal d3 stuff with axis values
    });

    // for max price line
    const firstPoint = data[0]
    const lastPoint = data[data.length - 1];
    const minRating = Math.min(...data.map(yRating));
    const maxRating = Math.max(...data.map(yRating));
    const maxPriceData = [
      { date: xRating(firstPoint), my_elo: maxRating },
      { date: xRating(lastPoint), my_elo: maxRating },
    ];

    return (
      <React.Fragment>
        <svg width={width} height={height} ref={s => (this.svg = s)}>
          <rect x={0} y={0} width={width} height={height} fill="#32deaa" rx={10} />
          <defs> {/* gradient defs */}
            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" stopColor="#fff" stopOpacity={1} />
              <stop offset="100%" stopColor="#fff" stopOpacity={0.2} />
            </linearGradient>
          </defs>
          <Group>
            <GridRows
              lineStyle={{ pointerEvents: 'none' }}
              scale={yScale}
              width={xMax}
              strokeDasharray="2,2"
              stroke="rgba(255, 255, 255, 0.3)"
            />
            <GridColumns
              lineStyle={{ pointerEvents: 'none' }}
              scale={xScale}
              height={yMax}
              strokeDasharray="2,2"
              stroke="rgba(255, 255, 255, 0.3)"
            />
            <MaxRating
              data={maxPriceData}
              yText={yScale(maxRating)}
              label="Highest Bullet Rating"
              x={d => xScale(xRating(d))}
              y={d => yScale(yRating(d))}
            />
            <AreaClosed
              data={data}
              x={d => xScale(xRating(d))}
              y={d => yScale(yRating(d))}
              yScale={yScale}
              strokeWidth={1}
              stroke={'url(#gradient)'}
              fill={'url(#gradient)'}
            />
            {/* add an 'overlay' that can handle tool tip data/events */}
            <Bar
              x={0}
              y={0}
              width={width}
              height={height - margin.bottom}
              fill="transparent"
              rx={10}
              data={data}
              onMouseMove={event =>
                this.handleTooltip({
                  event,
                  xRating,
                  xScale,
                  yScale,
                  data: data
                })
              }
              onMouseLeave={event => hideTooltip()}
            />
            {tooltipData && (
              <Group>
                <HoverLine
                  from={{ x: tooltipLeft, y: 0 }}
                  to={{ x: tooltipLeft, y: yMax }}
                  tooltipLeft={tooltipLeft}
                  tooltipTop={tooltipTop}
                />
              </Group>
            )}
          </Group>
        </svg>
          {tooltipData && (
            <Tooltip
              top={tooltipTop + 2}
              left={tooltipLeft + 2}
              style={{
                backgroundColor: 'rgba(92, 119, 235, 1)',
                color: 'white',
              }}
            >
              {`${yRating(tooltipData)}`}
            </Tooltip>
          )}
      </React.Fragment>
    );
  }

  render() {
    /**
     * sorting the data prior to charting is _very_ necessary. That would ideally
     * be done in the generation of the data set
     *
     * there's also a weird value right at the front of our list, which explains
     * the quick slice
     */
    const data = chessData.sort((a, b) => a.date - b.date).slice(1);

    return (
      <React.Fragment>
        {this.renderChart({ data })}
      </React.Fragment>
    );
  }
}

/**
 * Styles
 */
const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  padding-top: 10rem;
`;

const enhanced = compose(
  withScreenSize,
  withTooltip
);

export default enhanced(Chess);
