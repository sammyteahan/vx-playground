import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

const theme = {
  maxWidth: '1200px',
  colorMain: '#333',
  colorMainDark: '#27273f',
  colorAccent: '#EC407A',
  colorAccentDark: '#D23369',
  colorAccentLight: '#F9C5D7',
  colorGrey: '#D5D5D5',
  colorGreyDark: '#C8C8C8',
  colorGreyLight: '#F0F0F0',
  colorGradientStart: '#AA43E4',
  colorGradientMiddle: '#F65E88',
  colorGradientEnd: '#FFAF84',
  borderRadius: '6px',
};

/**
 * Base Page class to handle overall layout
 * and theming
 */
class Page extends React.Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        {this.props.children}
      </ThemeProvider>
    );
  }
}

export { Page as default };
