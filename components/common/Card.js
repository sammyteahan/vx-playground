import styled from 'styled-components';

const Card = styled.div`
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.7);
  border-radius: ${props => props.theme.borderRadius};
  background: ${({ theme }) => theme.colorMainDark};
  color: #fff;
  min-width: 500px;
  width: 1000px;

  h4 {
    font-size: 14px;
    letter-spacing: 1px;
    text-transform: uppercase;
  }
  h6 {
    font-size: 10px;
    letter-spacing: 1px;
    text-transform: uppercase;
  }
`;

const CardHeader = styled.div`
  padding: 1rem;
  border-bottom: 1px dashed rgba(255, 255, 255, 0.2);
`;

const CardBody = styled.div`
  padding: 12rem 1rem;
`;

const CardFooter = styled.div`
  padding: 1rem;
  border-top: 1px dashed rgba(255, 255, 255, 0.2);
`;

Card.Header = CardHeader;
Card.Body = CardBody;
Card.Footer = CardFooter;

export { Card as default };
