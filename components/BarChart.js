import React from 'react';

import * as d3 from 'd3';
import styled from 'styled-components';

import { getDummyData } from '../utils';

const BarChart = ({ data }) => {
  const chartRef = React.useRef(null);
  const colors = ['#2176ae', '#57b8ff', '#b66d0d', '#fbb13c', '#fe6847'];

  // setup chart width and margins
  // 800 widht is also a part of the parent containers max-width
  const margins = { top: 10, bottom: 20, left: 20, right: 20 };
  const width = 800 - margins.left - margins.right;
  const height = 500 - margins.top - margins.bottom;

  React.useEffect(() => draw(), [data]);

  const draw = () => {
    const svg = d3.select(chartRef.current);

    const xScale = d3
      .scaleBand()
      .domain(d3.range(0, data.length))
      .range([0, width]);

    const yScale = d3
      .scaleLinear()
      .domain([-3, 3])
      .range([0, height]);

    svg.selectAll('rect')
      .data(data)
      .transition()
      .duration(500)
      .attr('x', d => xScale(d.x))
      .attr('y', d => yScale(d.y))
      .attr('width', xScale.bandwidth())
      .attr('height', d => height - yScale(d.y))
      .style('fill', d => d.color);
  };

  // render initial bars, but without anything special to allow for
  // d3 to handle transition
  const bars = data.map(d => <rect key={d.name} />);

  return (
    <SvgContainer>
      <SvgContent
        ref={chartRef}
        preserveAspectRatio="xMinYMin meet"
        viewBox={`0 0 ${width} ${height}`}
      >
        {bars}
      </SvgContent>
    </SvgContainer>
  );
}

const SvgContainer = styled.div`
  display: inline-block;
  position: relative;
  width: 100%;
  height: 500px;
  overflow: hidden;
`;

const SvgContent = styled.svg`
  display: inline-block;
  position: absolute;
  top: 0;
  left: 0;
`;

export { BarChart as default };

