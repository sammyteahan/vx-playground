import { Group } from '@vx/group';
import { LinePath } from '@vx/shape';

const MaxRating = ({ data, label, yText, x, y, ...rest }) => (
  <Group>
    <LinePath
      data={data}
      x={x}
      y={y}
      stroke="rgba(92, 119, 235, 1)"
      strokeWidth={1}
      strokeDasharray="4,4"
      strokeOpacity=".8"
      {...rest}
    />
    <text fill="rgba(92, 119, 235, 1)" y={yText - 5} dx="5px" fontSize="12">
      {label}
    </text>
  </Group>
);

export { MaxRating as default };
