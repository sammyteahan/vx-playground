import React from 'react';

import { Group } from '@vx/group';
import { AreaClosed, Line, Bar } from '@vx/shape';

const HoverLine = ({ from, to, tooltipLeft, tooltipTop, ...rest }) => (
  <Group>
    {/* horizontal dashed line */}
    <Line
      from={from}
      to={to}
      stroke="rgba(92, 119, 235, 1)"
      strokeWidth={2}
      style={{ pointerEvents: 'none' }}
      strokeDasharray="2,2"
    />
    {/* circle(s) at elo */}
    <circle
      cx={tooltipLeft}
      cy={tooltipTop + 1}
      r={4}
      fill="black"
      fillOpacity={0.1}
      stroke="black"
      strokeOpacity={0.1}
      strokeWidth={2}
      style={{ pointerEvents: 'none' }}
    />
    <circle
      cx={tooltipLeft}
      cy={tooltipTop}
      r={4}
      fill="rgba(92, 119, 235, 1)"
      stroke="white"
      strokeWidth={2}
      style={{ pointerEvents: 'none' }}
    />
  </Group>
);

export { HoverLine as default };
